-- vim.lsp.set_log_level("debug")

local navic = require("nvim-navic")
local lspconfig = require("lspconfig")
local util = require("lspconfig/util")

-- nvim-cmp config
-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
-- (I think it works without this but whatever)
local updated_capabilities = require("cmp_nvim_lsp").default_capabilities()
local utf16_capabilities = require("cmp_nvim_lsp").default_capabilities()
utf16_capabilities.offsetEncoding = { "utf-16" }

-- attach
local custom_attach = function(client, bufnr)
    if client.server_capabilities.documentSymbolProvider then
        navic.attach(client, bufnr)
    end
    -- if vim.bo[bufnr].buftype ~= "" or vim.bo[bufnr].filetype == "helm" then
    if vim.bo[bufnr].buftype ~= "" then
        vim.diagnostic.enable(false)
    end
end

-- servers which require no config
local servers = {
    "ansiblels",
    "bashls",
    -- "cssls",
    -- "clangd",  -- extra-config
    "cmake",
    "cssmodules_ls",
    -- "emmet_ls",  -- extra-config
    "esbonio",
    -- fortls  -- extra-config
    "jsonls",
    "gopls",
    -- "helm_ls",  -- extra-config
    -- "html",
    -- "pylsp",  -- extra-config
    -- "basedpyright",  -- extra-config
    "ruff_lsp",
    -- "stylelint_lsp",
    -- "sumneko_lua",  -- extra-config
    "tailwindcss",
    "ts_ls",
    "taplo",
    "texlab",
    "vimls",
    "volar",
    -- "yamlls",  -- extra-config
}

for _, server in ipairs(servers) do
    lspconfig[server].setup({
        capabilities = updated_capabilities,
        on_attach = custom_attach,
    })
end

-- servers with extra configuration
-- pylsp, sumneko_lua, fortls, emmet_ls

-- We need to manually install: pylsp-mypy
lspconfig["pylsp"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    cmd = {
        "pylsp",
        "--check-parent-process",
    },
    settings = {
        pylsp = {
            plugins = {
                pycodestyle = { enabled = false },
                yapf = { enabled = false },
                autopep8 = { enabled = false },
                pyflakes = { enabled = false },
                flake8 = { enabled = false },
                mccabe = { enabled = false },
                pylint = { enabled = false },
                rope_autoimport = { enabled = false },
                black = { enabled = false },
                pyls_isort = { enabled = false },
                isort = { enabled = false, profile = "black" },
                ruff = { enabled = false },
            },
        },
    },
})

-- lspconfig["basedpyright"].setup({
--     settings = {
--         basedpyright = {
--             disableOrganizeImports = true,
--             analysis = {
--                 autoSearchPaths = true,
--                 diagnosticMode = "openFilesOnly",
--                 useLibraryCodeForTypes = true,
--                 -- typeCheckingMode = "strict",
--             },
--         },
--     },
-- })

-- for sumneko_lua
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig["lua_ls"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
                -- Setup your lua path
                path = runtime_path,
            },
            diagnostics = {
                enable = true,
                globals = {
                    -- Get the language server to recognize the `vim` global
                    "vim",
                },
            },
            workspace = {
                library = {
                    -- Make the server aware of Neovim runtime files
                    [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                    [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
                },
            },
            telemetry = {
                enable = false,
            },
        },
    },
})

lspconfig["fortls"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    cmd = {
        -- TODO: fix .exe for none windows
        vim.fn.stdpath("data") .. "/lsp_servers/fortls/venv/Scripts/fortls.exe",
        "--symbol_skip_mem",
        "--incrmental_sync",
        "--autocomplete_no_prefix",
        "--autocomplete_name_only",
        "--variable_hover",
    },
    root_dir = util.path.dirname,
})

lspconfig["emmet_ls"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    filetypes = {
        "html",
        "htmlhugo",
        "typescriptreact",
        "javascriptreact",
        "css",
        "sass",
        "scss",
        "less",
    },
})

-- clangd
lspconfig["clangd"].setup({
    cmd = { "clangd-19" },
    capabilities = utf16_capabilities,
    on_attach = custom_attach,
})

-- qmlls
lspconfig["qmlls"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    cmd = { "/home/panos/Qt/6.5.2/gcc_64/bin/qmlls" },
    filetypes = { "qlmjs", "qml" },
})
-- yamlls
lspconfig["yamlls"].setup({
    capabilities = updated_capabilities,
    on_attach = custom_attach,
    settings = {
        redhat = {
            telemetry = {
                enabled = false,
            },
        },
        yaml = {
            KeyOrdering = false,
            schemas = {
                ["https://raw.githubusercontent.com/argoproj/argo-workflows/master/api/jsonschema/schema.json"] = {
                    "workflow.yaml",
                    "argo-workflow-templates/*.yaml",
                },
                ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.31.2-standalone-strict/_definitions.json"] = "/*.k8s.yaml",
            },
        },
    },
})

-- helmls
-- if a helm file is opened before a yaml file, helm_ls is loaded
-- before yamlls, which means the detach will fail. However,
-- re-opening the file (`:e`) will properly detach the yamlls.
local helmls_attach = function(client, bufnr)
    for _, server in ipairs(vim.lsp.get_clients()) do
        if server.name == "yamlls" then
            vim.lsp.buf_detach_client(bufnr, server.id)
        end
    end
    -- not supported yet (?)
    if client.server_capabilities.documentSymbolProvider then
        navic.attach(client, bufnr)
    end
end
lspconfig["helm_ls"].setup({
    capabilities = updated_capabilities,
    on_attach = helmls_attach,
})

-- none-ls
local null_ls = require("null-ls")
null_ls.setup({
    sources = {
        -- lua
        null_ls.builtins.formatting.stylua.with({ extra_args = { "--indent-type", "Spaces" } }),
        -- Python
        null_ls.builtins.formatting.isort.with({
            extra_args = { "--profile", "black" },
        }),
        null_ls.builtins.formatting.packer,
        -- clang
        -- null_ls.builtins.formatting.clang_format.with({ extra_args = { "-style", "WebKit" } }),
        null_ls.builtins.formatting.clang_format,
        -- cmake
        null_ls.builtins.diagnostics.cmake_lint,
        null_ls.builtins.formatting.cmake_format,
        -- prettier
        null_ls.builtins.formatting.prettier.with({
            extra_filetypes = { "htmlhugo" },
        }),
        -- yaml
        null_ls.builtins.formatting.yamlfix,
        -- qml
        null_ls.builtins.formatting.qmlformat.with({
            command = "/home/panos/Qt/6.5.2/gcc_64/bin/qmlformat",
        }),
        -- sql
        null_ls.builtins.formatting.sqlfluff,
        null_ls.builtins.diagnostics.sqlfluff,
    },
})
