local M = {}

local navic = require("nvim-navic")

M.navic_virt_text = function()
    if navic.is_available() then
        local virt_txt = navic.get_location()
        if virt_txt ~= "" then
            local bnr = vim.fn.bufnr("%")
            local ns_id = vim.api.nvim_create_namespace("navic")

            local line_num, col_num = unpack(vim.api.nvim_win_get_cursor(0))

            local opts = {
                end_row = 0,
                id = 1,
                virt_text = { { virt_txt, "NormalFloat" } },
                virt_text_pos = "eol",
            }

            vim.api.nvim_buf_set_extmark(bnr, ns_id, line_num - 1, col_num, opts)
        end
    end
end

return M
