-- vim:fdm=marker:foldlevel=0
local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local f = ls.function_node
local i = ls.insert_node
-- local d = ls.dynamic_node
-- local sn = ls.snippet_node
local isn = ls.indent_snippet_node
-- local c = ls.choice_node
-- local events = require("luasnip.util.events")
-- local fmt = require("luasnip.extras.fmt").fmt
local types = require("luasnip.util.types")

-- {{{ setup
ls.config.setup({
    store_selection_keys = "<Tab>",
    ext_opts = {
        [types.choiceNode] = {
            active = {
                virt_text = { { "choiceNode", "Comment" } },
            },
        },
    },
})
-- }}}

-- {{{ functions
local function copy(args)
    return args[1]
end

local function filename_base_upper(_, snip)
    return string.upper(snip.env.TM_FILENAME_BASE)
end
-- }}}

ls.add_snippets(nil, {
    all = { -- {{{
        -- trigger is fn.
        s("fn", {
            -- Simple static text.
            t("//Parameters: "),
            -- function, first parameter is the function, second the Placeholders
            -- whose text it gets as input.
            f(copy, 2),
            t({ "", "function " }),
            -- Placeholder/Insert.
            i(1),
            t("("),
            -- Placeholder with initial text.
            i(2, "int foo"),
            -- Linebreak
            t({ ") {", "\t" }),
            -- Last Placeholder, exit Point of the snippet. EVERY 'outer' SNIPPET NEEDS Placeholder 0.
            i(0),
            t({ "", "}" }),
        }),
    }, -- }}}
    ansible = { --{{{
        s("printd", {
            t({
                "- name: Test",
                "  tags: test",
                "  ansible.builtin.debug:",
                '    msg: "{{ ',
            }),
            i(1),
            t(' }}"'),
        }),
        s("testb", {
            t({
                "- name: Test block",
                "  tags: test",
                "  block:",
            }),
        }),
    }, --}}}
    vim = { -- {{{
        s("echom", {
            t('echom "'),
            f(copy, 1),
            t(' = " . '),
            i(1, "var"),
        }),
    }, --}}}
    python = { --{{{
        s("#!", {
            t({
                "#!/usr/bin/env python3",
                "# -*- coding: utf-8 -*-",
            }),
        }),
        s("ifmain", {
            t({
                'if __name__ == "__main__":',
                "\t",
            }),
            i(1, "main()"),
        }),
        s("ipy", { t("from IPython import embed; embed()") }),
        s("ipdb", { t("import ipdb; ipdb.set_trace()") }),
        s("exit", { t("raise SystemExit") }),
        s("doc", { t([["""]]), i(1), t([["""]]), i(0) }),
        s("print", { t([[print("]]), i(1), t([[")]]), i(0) }),
        s("printd", { t([[print(f"{]]), i(1), t([[ = }")]]), i(0) }),
        s("gc", {
            t({
                "import gc",
                "for obj in gc.get_objects():",
                "\tif isinstance(obj, ",
            }),
            i(1, "some_class"),
            t({
                "):",
                "\t\tprint(obj)",
            }),
        }),
    }, --}}}
    fortran = { --{{{
        s("print", { t([[write(*,*) "]]), f(copy, 1), t([[ = ", ]]), i(1) }),
        s("pause", {
            t({
                [[write(*,*) "Press ENTER to continue."]],
                "read(*,*)",
            }),
        }),
        s("if", {
            t("if ("),
            i(1, "conditional"),
            t({ ") then", "\t" }),
            isn(2, {
                f(function(_, snip)
                    return snip.env.SELECT_DEDENT
                end, {}),
            }, "\t$PARENT_INDENT"),
            i(3),
            t({ "", "endif" }),
        }),
    }, --}}}
    lua = { --{{{
        s("printt", {
            t("for i, j in pairs("),
            i(1, "mytable"),
            t({
                ") do",
                '\tprint(string.format("',
            }),
            f(copy, 1),
            t({
                '[%s] = %s", i, j))',
                "end",
            }),
        }),
    }, --}}}
    tex = { --{{{
        s("in", { t([[\(]]), i(1), t([[\)]]) }),
        s("~", { t([[\widetilde{]]), i(1), t("}") }),
        s("frac", { t([[\frac{]]), i(1), t("}{"), i(2), t("}") }),
        s("fracp", { t([[\frac{\partial ]]), i(1), t([[}{\partial ]]), i(2), t("}") }),
        s("eps", { t([[\widetilde{\epsilon}]]) }),
        s("epsk", { t([[\frac{\widetilde{\epsilon}}{\widetilde{k}}]]) }),
        s("keps", { t([[\frac{\widetilde{k}}{\widetilde{\epsilon}}]]) }),
        s("barr", { t([[\bar{\rho}]]) }),
        s("citet", { t([[\citet{]]), i(1), t("}") }),
        s("citep", { t([[\citep{]]), i(1), t("}") }),
        s("eq", {
            t({ [[\begin{equation}]], "\t" }),
            i(1, "a = b + c"),
            t({ "", [[\end{equation}]] }),
        }),
        s("eql", {
            t({ [[\begin{equation}]], "\t" }),
            i(2, "a = b + c"),
            t({ "", "\t\\label{eq:" }),
            i(1, "label"),
            t({ "}", [[\end{equation}]] }),
        }),
        s(
            { trig = "_(%d)", regTrig = true, wordTrig = false },
            f(function(_, snip)
                return "\\textsubscript{" .. snip.captures[1] .. "}"
            end, {})
        ),
    }, --}}}
    htmlhugo = { --{{{
        s("printd", { t('{{ print "%#v" '), i(1), t(" }}"), i(0) }),
    }, --}}}
    cpp = { --{{{
        s("ifnd", {
            t("#ifndef "),
            f(filename_base_upper),
            t({ "_H", "#define " }),
            f(filename_base_upper),
            t({ "_H", "", "" }),
            i(0),
            t({ "", "", "#endif  // !" }),
            f(filename_base_upper),
            t("_H"),
        }),
    }, --}}}
})
