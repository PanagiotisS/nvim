return {
    --
    {
        "lifepillar/vim-solarized8",
        lazy = false,
        priority = 1000,
        branch = "neovim",
        config = function()
            vim.g.solarized_diffmode = "normal"
            vim.g.solarized_italics = 1
            vim.cmd.colorscheme("solarized8")
        end,
    },
    --
    {
        "bfredl/nvim-miniyank",
        event = "BufEnter",
        keys = {
            { "p",     "<Plug>(miniyank-autoput)",   mode = "" },
            { "P",     "<Plug>(miniyank-autoPut)",   mode = "" },
            { "<c-p>", "<Plug>(miniyank-cycle)",     mode = "" },
            { "<c-n>", "<Plug>(miniyank-cycleback)", mode = "" },
        },
        init = function()
            vim.g.miniyank_maxitems = 10
        end,
    },
    --
    {
        "mhinz/vim-signify",
        init = function()
            vim.o.updatetime = 100
            vim.g.signify_sign_change = "~"
            local signify_augroup = vim.api.nvim_create_augroup("signify_augroup", { clear = true })
            vim.api.nvim_create_autocmd({ "ColorScheme" }, {
                pattern = "*",
                group = signify_augroup,
                command = "highlight SignifySignAdd ctermfg=242 guifg=#859900",
            })
            vim.api.nvim_create_autocmd({ "ColorScheme" }, {
                pattern = "*",
                group = signify_augroup,
                command = "highlight SignifySignChange ctermbg=248 guifg=#b58900",
            })
            vim.api.nvim_create_autocmd({ "ColorScheme" }, {
                pattern = "*",
                group = signify_augroup,
                command = "highlight SignifySignDelete ctermfg=12 ctermbg=242 guifg=#dc322f",
            })
        end,
    },
    {
        "preservim/vim-indent-guides",
        init = function()
            vim.g.indent_guides_enable_on_vim_startup = 1
            vim.g.indent_guides_guide_size = 1
            vim.g.indent_guides_default_mapping = 0
            -- see also `after/ftplugin/fortran.vim`
        end,
    },
    {
        "kassio/neoterm",
        cmd = { "Tnew", "T", "Texec", "Topen", "Ttoggle" },
        keys = {
            { "<leader>nk", "<cmd>Tkill<cr>",                 mode = "n", remap = true },
            { "<leader>nn", "<Plug>(neoterm-repl-send-line)", mode = "n", remap = true },
            { "<leader>nn", "<Plug>(neoterm-repl-send)",      mode = "x", remap = true },
        },
        init = function()
            vim.g.neoterm_default_mod = "belowright"
            vim.g.neoterm_size = 10
            vim.g.neoterm_fixedsize = 1
            vim.g.neoterm_autoscroll = 1
            if vim.fn.has("win32") == 1 then
                vim.g.neoterm_use_relative_path = 1
                vim.g.neoterm_eof = "\r"
                vim.g.neoterm_shell = vim.fn.executable("pwsh") == 1 and "pwsh" or "powershell"
                vim.o.shell = "cmd.exe"
            end
        end,
    },
    {
        "nvim-lualine/lualine.nvim",
        opts = {
            options = {
                icons_enabled = false,
                section_separators = { left = "", right = "" },
                component_separators = { left = "", right = "" },
            },
            sections = {
                lualine_b = {
                    { "filename", path = 1 },
                },
                lualine_c = {
                    "branch",
                    "diff",
                    "diagnostics",
                },
            },
        },
    },
    -- functionality
    { "nvim-lua/plenary.nvim" },
    { "tpope/vim-repeat" },
    {
        "rafamadriz/friendly-snippets",
        lazy = true,
    },
    -- { "Konfekt/FastFold" },
    { "PanagiotisS/LargeFile" },
    -- filetypes
    -- { "octol/vim-cpp-enhanced-highlight" },
    "towolf/vim-helm",
    "phelipetls/vim-hugo",
    {
        "pearofducks/ansible-vim",
        lazy = true,
        ft = "yaml",
    },
    -- Lazy
    {
        "guns/xterm-color-table.vim",
        lazy = true,
        cmd = "XtermColorTable",
    },
    {
        "mbbill/undotree",
        lazy = true,
        cmd = "UndotreeToggle",
        init = function()
            vim.g.undotree_WindowLayout = 3
            vim.g.undotree_ShortIndicators = 1
        end,
    },
    {
        "panozzaj/vim-autocorrect",
        lazy = true,
        ft = "tex",
    },
    {
        "ron89/thesaurus_query.vim",
        lazy = true,
        ft = "tex",
        init = function()
            vim.g.tq_openoffice_en_file = "/usr/share/mythes/th_en_GB_v2"
            vim.g.tq_mthesaur_file = "~/.config/nvim/thesaurus/mthesaur.txt"
        end,
    },
    {
        "godlygeek/tabular",
        lazy = true,
        ft = "tex",
    },
    {
        "will133/vim-dirdiff",
        lazy = true,
        cmd = "DirDiff",
        init = function()
            vim.g.DirDiffExcludes = ".git,bundle,*.swp,*.dat,*.hdf,*.o,*.png,*.pdf,*.aux"
            vim.g.DirDiffWindowSize = 10
            -- see also `after/ftplugin/dirdiff.vim`
        end,
    },
    {
        "tpope/vim-fugitive",
        lazy = true,
        cmd = { "Git", "Gwrite", "Gvdiffsplit", "Gread" },
        -- see also `after/ftplugin/fugitive.vim`
    },
    {
        "tpope/vim-surround",
        lazy = true,
        event = "InsertEnter",
    },
    {
        "jpalardy/vim-slime",
        lazy = true,
        cmd = { "SlimeRegionSend", "SlimeLineSend", "SlimeParagraphSend", "SlimeConfig" },
        init = function()
            vim.g.slime_target = "tmux"
            vim.g.slime_no_mappings = 1
        end,
        keys = {
            { "<leader>ss", "<Plug>SlimeRegionSend",    mode = "x", remap = true },
            { "<leader>ss", "<Plug>SlimeLineSend",      mode = "n", remap = true },
            { "<leader>sp", "<Plug>SlimeParagraphSend", mode = "n", remap = true },
            { "<leader>sc", "<Plug>SlimeConfig",        mode = "n", remap = true },
        },
        -- see also `after/ftplugin/python.vim`
    },
    {
        "tweekmonster/startuptime.vim",
        lazy = true,
        cmd = "StartupTime",
    },
    {
        "numToStr/Comment.nvim",
        lazy = true,
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        event = "BufEnter",
        config = true,
    },
    {
        "j-hui/fidget.nvim",
        enabled = false,
        lazy = true,
        event = "BufEnter",
        ft = { "fortran", "tex", "lua", "vim", "python" },
        -- opts = {
        --     notification = {
        --         override_vim_notify = true,
        --     },
        -- },
    },
    {
        "brenoprata10/nvim-highlight-colors",
        event = "InsertEnter",
        config = function()
            require("nvim-highlight-colors").setup({
                render = "background", -- or 'foreground' or 'first_column'
                enable_tailwind = true,
            })
        end,
    },
    {
        "SmiteshP/nvim-navic",
        dependencies = "nvim-lualine/lualine.nvim",
        config = function()
            local navic_augroup = vim.api.nvim_create_augroup("navic_augroup", { clear = true })
            vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                pattern = "*",
                group = navic_augroup,
                -- command = "lua require'navic_virt_text'.navic_virt_text()",
                command = "lua require'navic_virt_text'.navic_virt_text()",
            })
            local navic = require("nvim-navic")
            navic.setup({
                icons = {
                    File = "",
                    Module = "",
                    Namespace = "",
                    Package = "",
                    Class = "",
                    Method = "",
                    Property = "",
                    Field = "",
                    Constructor = "",
                    Enum = "",
                    Interface = "",
                    Function = "",
                    Variable = "",
                    Constant = "",
                    String = "",
                    Number = "",
                    Boolean = "",
                    Array = "",
                    Object = "",
                    Key = "",
                    Null = "",
                    EnumMember = "",
                    Struct = "",
                    Event = "",
                    Operator = "",
                    TypeParameter = "",
                },
            })

            require("lualine").setup({
                sections = {
                    lualine_c = {
                        "branch",
                        "diff",
                        "diagnostics",
                        { navic.get_location, cond = navic.is_available },
                    },
                },
            })
        end,
    },
    -- cmp
    {
        "hrsh7th/cmp-nvim-lsp",
        lazy = true,
    },
    {
        "hrsh7th/cmp-buffer",
        lazy = true,
    },
    {
        "hrsh7th/cmp-path",
        lazy = true,
    },
    {
        "saadparwaiz1/cmp_luasnip",
        lazy = true,
        dependencies = { "L3MON4D3/LuaSnip" },
    },
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "saadparwaiz1/cmp_luasnip",
        },
        event = "InsertEnter",
        config = function()
            require("my-cmp-config")
        end,
    },
    -- luasnip
    {
        "L3MON4D3/LuaSnip",
        lazy = true,
        dependencies = { "rafamadriz/friendly-snippets" },
        config = function()
            require("myluasnippets")
            require("luasnip.loaders.from_vscode").lazy_load()
        end,
        keys = {
            {
                "<c-k>",
                function()
                    if require("luasnip").expand_or_jumpable() then
                        require("luasnip").expand_or_jump()
                    end
                end,
                mode = "i",
            },
            {
                "<c-k>",
                function()
                    require("luasnip").jump(1)
                end,
                mode = "s",
            },
            {
                "<c-j>",
                function()
                    require("luasnip").jump(-1)
                end,
                mode = { "s", "i" },
            },
            {
                "<c-e>",
                function()
                    if 'require("luasnip").choice_active()' then
                        require("luasnip").change_choice(1)
                    end
                end,
                mode = { "i", "s" },
            },
        },
    },
    -- lsp
    {
        "neovim/nvim-lspconfig",
        lazy = false,
        cmd = "LspInfo",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "williamboman/mason-lspconfig.nvim",
            "SmiteshP/nvim-navic",
            "nvimtools/none-ls.nvim",
            "j-hui/fidget.nvim",
        },
        init = function()
            vim.keymap.set("n", "<leader>lp", "<cmd>lua print(vim.inspect(vim.lsp.buf_get_clients()))<CR>")
        end,
        config = function()
            require("my-nvim-lsp-installer")
        end,
        keys = {
            {
                "<leader>k",
                function()
                    vim.lsp.buf.hover()
                end,
                desc = "vim.lsp.buf.hover",
            },
            {
                "<leader>ld",
                function()
                    vim.lsp.buf.definition()
                end,
                desc = "vim.lsp.buf.definition",
            },
            {
                "<leader>lh",
                function()
                    vim.diagnostic.open_float({ source = "always" })
                end,
                desc = "vim.diagnostic.open_float",
            },
            {
                "<leader>lf",
                function()
                    vim.lsp.buf.format({ async = true })
                end,
                desc = "vim.lsp.buf.format",
            },
            {
                "<leader>la",
                function()
                    vim.lsp.buf.code_action()
                end,
                desc = "vim.lsp.buf.code_action",
            },
            {
                "<leader>l[",
                function()
                    vim.diagnostic.goto_prev()
                end,
                desc = "vim.diagnostic.goto_prev",
            },
            {
                "<leader>l]",
                function()
                    vim.diagnostic.goto_next()
                end,
                desc = "vim.diagnostic.goto_next",
            },
        }, -- end keys
    },
    {
        "nvimtools/none-ls.nvim",
        lazy = true,
    },
    {
        "williamboman/mason.nvim",
        lazy = true,
        config = function()
            require("mason").setup({
                registries = {
                    "github:mason-org/mason-registry",
                },
            })
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        lazy = true,
        dependencies = { "williamboman/mason.nvim" },
        cmd = { "Mason", "MasonInstall", "MasonLog", "MasonUninstall", "MasonUninstallAll" },
        opts = { automatic_installation = true },
    },
    -- treesitter
    {
        "nvim-treesitter/nvim-treesitter",
        lazy = true,
        cond = "vim.fn.has('nvim-0.6')",
        event = "BufRead",
        build = ":TSUpdate",
        config = function()
            local tsft = {
                "bash",
                "bibtex",
                "c",
                "cmake",
                "cpp",
                "fortran",
                "html",
                "json",
                -- "latex",
                "lua",
                "markdown",
                "markdown_inline",
                "python",
                "qmljs",
                "query",
                "regex",
                "rst",
                "sql",
                "toml",
                "vim",
                "vimdoc",
                "yaml",
            }
            require("nvim-treesitter.configs").setup({
                ensure_installed = { tsft },
                highlight = {
                    enable = true,
                    disable = {},
                },
            })
            local ts_augroup = vim.api.nvim_create_augroup("ts_fold", { clear = true })
            vim.api.nvim_create_autocmd({ "FileType" }, {
                pattern = tsft,
                group = ts_augroup,
                callback = function()
                    vim.opt_local.foldexpr = "v:lua.vim.treesitter.foldexpr()"
                    vim.opt_local.foldmethod = "expr"
                    vim.opt_local.spelllang = "en_gb"
                end,
            })
        end,
    },
    {
        "nvim-treesitter/playground",
        lazy = true,
        cmd = "TSPlaygroundToggle",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
    },
    {
        "nvim-treesitter/nvim-treesitter-context",
        lazy = true,
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        event = "BufRead",
        config = function()
            require("treesitter-context").setup({
                enable = true,
                throttle = true,
                max_lines = 5,
                multiline_threshold = 2,
                patterns = {
                    default = {
                        "class",
                        "function",
                        "method",
                        "for",
                        "while",
                        "if",
                        "switch",
                        "case",
                    },
                },
            })
        end,
    },
    {
        "nvim-treesitter/nvim-treesitter-textobjects",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        event = "BufRead",
        config = function()
            require("nvim-treesitter.configs").setup({
                textobjects = {
                    select = {
                        enable = true,
                        lookahead = true,
                        keymaps = {
                            ["af"] = "@function.outer",
                            ["if"] = "@function.inner",
                            ["ac"] = "@class.outer",
                            ["ic"] = "@class.inner",
                        },
                    },
                    move = {
                        enable = true,
                        set_jumps = true,
                        goto_next_start = {
                            ["]f"] = "@function.outer",
                            ["]o"] = "@class.outer",
                        },
                        goto_next_end = {
                            ["]F"] = "@function.outer",
                            ["]O"] = "@class.outer",
                        },
                        goto_previous_start = {
                            ["[f"] = "@function.outer",
                            ["[o"] = "@class.outer",
                        },
                        goto_previous_end = {
                            ["[F"] = "@function.outer",
                            ["[O"] = "@class.outer",
                        },
                    },
                    lsp_interop = {
                        enable = true,
                        border = "none",
                        peek_definition_code = {
                            ["<leader>df"] = "@function.outer",
                            ["<leader>dF"] = "@class.outer",
                        },
                    },
                },
            })
        end,
    },
    -- Telescope
    {
        "nvim-telescope/telescope.nvim",
        lazy = true,
        cmd = "Telescope",
        event = "BufEnter",
        dependencies = {
            "nvim-telescope/telescope-fzf-native.nvim",
            "hrsh7th/nvim-cmp",
            "telescope-file-browser.nvim",
            "telescope-git-diffs.nvim",
        },
        config = function()
            require("telescope").setup({
                defaults = {
                    mappings = {
                        i = {
                            ["<C-s>"] = "select_horizontal",
                            ["<C-x>"] = require("telescope.actions").delete_buffer,
                        },
                        n = {
                            ["<C-s>"] = "select_horizontal",
                            ["<C-c>"] = "close",
                            ["<C-x>"] = require("telescope.actions").delete_buffer,
                        },
                    },
                    layout_strategy = "flex",
                    layout_config = {
                        flex = {
                            flip_columns = 120,
                        },
                    },
                },
                extensions = {
                    file_browser = {
                        dir_icon = "📁",
                    },
                },
            })

            require("telescope").load_extension("fzf")
            require("telescope").load_extension("file_browser")
            require("telescope").load_extension("git_diffs")
        end,
        keys = {
            {
                "<leader>gr",
                function()
                    require("telescope.builtin").live_grep({})
                end,
                desc = "Telescope interactive grep",
            },
            {
                "<leader>gw",
                function()
                    require("telescope.builtin").grep_string({ word_match = "-w" })
                end,
                desc = "Telescope grep the exact word under cursor",
            },
            {
                "<leader>ge",
                function()
                    require("telescope.builtin").grep_string({})
                end,
                desc = "Telescope grep the word under cursor",
            },
            {
                "<leader>r",
                function()
                    require("telescope.builtin").find_files({})
                end,
                desc = "Telescope find_files",
            },
            {
                "<leader>b",
                function()
                    require("telescope.builtin").buffers({ show_all_buffers = true })
                end,
                desc = "Telescope buffers",
            },
            {
                "<leader>lr",
                function()
                    require("telescope.builtin").lsp_references({})
                end,
                desc = "Telescope lsp_references",
            },
            {
                "<leader>tr",
                function()
                    require("telescope.builtin").resume({})
                end,
                desc = "Telescope resume",
            },
            {
                "<silent><leader>f",
                function()
                    require("telescope").extensions.file_browser.file_browser()
                end,
                desc = "Telescope file browser",
            },
        },
    },
    {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        lazy = true,
    },
    {
        "nvim-telescope/telescope-file-browser.nvim",
        lazy = true,
    },
    {
        "paopaol/telescope-git-diffs.nvim",
        lazy = true,
        dependencies = { "sindrets/diffview.nvim" },
    },
    --
    {
        "sindrets/diffview.nvim",
        lazy = true,
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("diffview").setup({
                use_icons = false,
                signs = {
                    fold_closed = "📁",
                    fold_open = "📂",
                    done = "✓",
                },
            })
        end,
    },
}
