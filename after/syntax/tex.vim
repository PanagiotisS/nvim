" match
" syntax match texSubscript "_j" contained conceal cchar=ⱼ
"syntax match texSubscripts "j" contained conceal cchar=ⱼ nextgroup=texSubscripts

"syntax match texSubscript "_l" contained conceal cchar=ₗ
"syntax match texSubscripts "l" contained conceal cchar=ₗ nextgroup=texSubscripts

syntax match texTypeStyle "\\mathcal{F}" conceal cchar=ℱ
syntax match texTypeStyle "\\nicefrac{1}{2}" conceal cchar=½
syntax match texTypeStyle "\\frac{1}{2}" conceal cchar=½
syntax match texTypeStyle "\\nicefrac{1}{4}" conceal cchar=¼
syntax match texTypeStyle "\\frac{1}{4}" conceal cchar=¼
syntax match texTypeStyle "\\nicefrac{3}{4}" conceal cchar=¾
syntax match texTypeStyle "\\frac{3}{4}" conceal cchar=¾

"syntax match texTypeStyle "\\underline{\\Psi}" conceal cchar=Ψ̲

syntax sync maxlines=2000
syntax sync minlines=500
syntax sync fromstart
syntax spell toplevel
