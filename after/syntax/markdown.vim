syntax match MyToDo /^\[x\]/ conceal cchar=✘
syntax match MyToDo /^\[\ \]/ conceal cchar=□
