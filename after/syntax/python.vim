" VIM syntax file
" Language: Python
"
" Highlighting Groups
" No need, all needed, are defined in vimrc

" Highlight Rules
" Indent Guide Lines
" syntax match indentGuide '\%(\_^\s\{}\)\@<=\%(\%1v\|\%5v\|\%9v\|\%13v\)\s'

" Highlight keyword CARE if is in comments
syntax keyword careGroup CARE TODO contained
syntax match comment /#.*/ contains=careGroup

" link Groups with Colours
hi def link OverLengthGrp     OverLengthHL
hi def link indentGuide       indentGuideLinesHL
hi def link careGroup         Todo
