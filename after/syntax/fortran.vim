" VIM syntax file
" Language: Fortran (additional rules for indent guide lines)
" Last Change: Jul 16, 2015

execute 'highlight Fortran1 ctermbg=254 guibg=' . CalculateColourTint(synIDattr(hlID("Normal"), "bg"), 0.15)

" Highlighting Groups
" highlight Fortran1 ctermbg=254 guibg=#e4e4e4

" Highlight Rules
if exists("b:fortran_fixed_source")
    if (b:fortran_fixed_source) == 0
        syn match OverLengthGroup    '\%80v.\+'
        " syn match indentGuide        '\%(\_^\s\{}\)\@<=\%(\%1v\|\%5v\|\%9v\|\%13v\)\s'
    else
        syn match OverLengthGroup    '\%73v.\+'
        " syn match indentGuide        '\%(\%7c\s\{}\)\@<=\%(\%7v\|\%10v\|\%13v\|\%16v\|\%19v\|\%22v\|\%25v\|\%28v\)\s'
        syn match fixedFortranGuides '\%1v\s'
        syn match fixedFortranGuides '\%2v\s\{5}'
    endif
endif

syn keyword careGroup CARE TODO contained
syntax match comment /!.*/    contains=careGroup,@Spell
syntax match comment /\_^C.*/ contains=careGroup,@Spell

" link Groups with Colours
hi def link OverLengthGroup     OverLengthHL
hi def link indentGuide         indentGuideLinesHL
hi def link fixedFortranGuides  Fortran1
hi def link careGroup           Todo

" openmp
syntax match fortranDirective "\v!\$\s"
syn region fortranDirective start=/!$omp.\{-}/ end=/[^\&]$/
syn region fortranDirective start=/!DIR$.\{-}/ end=/[^\&]$/
