if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F5> :T gnuplot %<cr>
else
    nnoremap <buffer> <F5> :split term://gnuplot '%' <cr>
endif
