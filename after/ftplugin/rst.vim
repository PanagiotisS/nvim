setlocal spell spelllang=en_gb
if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F5> :T make html<cr>
else
    nnoremap <buffer> <F5> :split term://make html <cr>
endif
