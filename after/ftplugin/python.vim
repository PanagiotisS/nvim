" setlocal foldmethod=indent
setlocal colorcolumn=81

let b:slime_bracketed_paste = 1

if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F5> :T python3 %<cr>
    nnoremap <buffer> <F6> :T ipython3 %<cr>
else
    nnoremap <buffer> <F5> :split term://python3  '%' <cr>
    nnoremap <buffer> <F6> :split term://ipython3 '%' <cr>
endif
