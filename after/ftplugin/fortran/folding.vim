" Folding rules for Fixed Format Fortran (e.g. 77)
" Eventually this folding also got a bit slow, for big files
" However, if it is used with vim-FastFold plugin is fine
if exists('b:fortran_fixed_source')
    if (b:fortran_fixed_source) == 1
        setlocal foldmethod=expr
        setlocal foldexpr=Fortran77Fold(v:lnum)
        setlocal foldcolumn=4
        setlocal foldtext=Fortran77FoldText()

        function! Fortran77Fold(lnum)
            let l:testLine = getline(a:lnum)
            let l:testLine2 = getline(a:lnum + 1)
            let l:subLevel  = 0

            " Check for the next Line
            " Check for else
            if l:testLine2 =~? '\_^\(C|!\)\@<!\s*else'
                let l:subLevel -= 1
                " return 's1'
            endif

            " Check for this line
            " Skip Comments and empty lines
            if l:testLine =~? '\v^\s*$'
                let l:subLevel = l:subLevel
                " return '='
            elseif l:testLine =~? '^C'
                let l:subLevel = l:subLevel
                "return '='
            elseif l:testLine =~? '\_^\s*!'
                let l:subLevel = l:subLevel
                " return '='

            " Remove comments
            let l:testLine = split(l:testLine, '!')[0]

            " Check for if/else/endif
            elseif l:testLine =~? '\%>6c\s*end\s*if'
                let l:subLevel -= 1
                " return 's1'
            elseif l:testLine =~? '\%>6c\s*else'
                let l:subLevel += 1
                " return 'a1'
            elseif l:testLine =~? '\%>6c\s*if'
                if l:testLine =~? '\%(\%>6c\s*if.*\)\zethen'
                    let l:subLevel += 1
                " test if 'if' continues to next lines
                else
                    " if line continues to the next line
                    " (there is a non white character at 6th column)
                    let l:nextLine = 1
                    while getline(a:lnum + l:nextLine) =~? '\%6c\S'
                        if getline(a:lnum + l:nextLine) =~? 'then'
                            let l:subLevel += 1
                        endif
                        let l:nextLine += 1
                    endwhile
                endif
                " return 'a1'

            " Check for do/enddo,
            " ignore do/continue e.g do 1000 j= ...
            elseif l:testLine =~? '\%>6c\s*end\s\=do'
                let l:subLevel -= 1
                " return 's1'
            " match 'do' if, after column 6, there is whitespace only
            " preceding and an alphabetic character succeeding
            elseif l:testLine =~? '\%(\(\%>6c\S\+\s*\)\@<!do\s\+\)\@>\a'
                let l:subLevel += 1
                " return 'a1'

            " Check for program or subroutine or function
            elseif l:testLine =~? '\_^\s*program'
                return '>1'
            elseif l:testLine =~? '\_^\s*subroutine'
                return '>1'
            elseif l:testLine =~? '\_^\s*\(integer\|double\s*precision\)\s*function'
                return '>1'
            elseif l:testLine =~? '\_^\s*block'
                return '>1'
            endif

            " Return
            if l:subLevel == 0
                return '='
            elseif l:subLevel < 0
                return 's' . -l:subLevel
            elseif l:subLevel > 0
                return 'a' . l:subLevel
            endif
            return '='
        endfunction

        function! Fortran77FoldText()
            let l:foldsize = (v:foldend-v:foldstart)
            return getline(v:foldstart).' ['.l:foldsize.' lines]'
        endfunction
    else
        set foldmethod=syntax
        let b:fortran_fold=1
        let b:fortran_fold_conditionals=1
        let b:fortran_more_precise=1
    endif
endif
