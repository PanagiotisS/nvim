if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F5> :T rm -rf public; hugo <cr>
else
    nnoremap <buffer> <F5> :split term://hugo <cr>
endif
