if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F5> :T /usr/bin/bash %<cr>
else
    nnoremap <buffer> <F5> :split term:///usr/bin/bash '%' <cr>
endif
