setlocal foldmethod=indent

if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F6> :T go run %<cr>
else
    nnoremap <buffer> <F6> :split term://go run '%' <cr>
endif
