setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2

" Custom Compiling Options
function! CustomCompile()
    if has('win32')
        if filereadable(expand('%:r').'.exe')
            :!DEL %:r.exe
        endif
        :!g++ -std=c++11 -g -Wall -Wextra -D_GLIBCXX_DEBUG -o %:r.exe %
    elseif has('mac')
        silent !clear
        :!rm %:r.exe
        :!g++-5 -std=c++11 -g -Wall -Wextra -D_GLIBCXX_DEBUG -o %:r.exe %
    else
        silent !clear
        :!rm %:r.exe
        :!g++ -std=c++11 -g -Wall -Wextra -D_GLIBCXX_DEBUG -o %:r.exe %
    endif
endfunction
" Check if there is a Makefile
function! CheckMakeFile()
    if filereadable(expand('%:p:h').'/Makefile')
        make
    else
        call CustomCompile()
    endif
endfunction
" Run the executable
function! RunExecutable()
    if filereadable(expand('%:r').'.exe')
        if has('win32')
            :!%:r.exe
        else
            silent !clear
            :!./%:r.exe
        endif
    else
        echom 'Sorry '.expand('%:r').'.exe not found'
        " Check if there is a Makefile and if there is one try to
        " run make run
        if filereadable(expand('%:p:h').'/Makefile')
            :!make run
        endif
    endif
endfunction
"
nnoremap <F5> :call CheckMakeFile()<CR>
nnoremap <F6> :call RunExecutable()<CR>
