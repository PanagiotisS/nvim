" If fortran 77
if exists("b:fortran_fixed_source")
    if (b:fortran_fixed_source) == 1
        " Set tab equal to 3 spaces for fortran files
        setlocal shiftwidth=3
        setlocal tabstop=3
        setlocal colorcolumn=73

        " for nathanaelkane/vim-indent-guides
        let g:indent_guides_start_level=3
    else
        setlocal colorcolumn=81
    endif
endif

function! SelectFortranCompiler(...)
    " If no inputs where given then set default value to 0
    let l:FCFlags = a:0 > 0 ? a:1 : 0
    if l:FCFlags == 1
        if executable('pgfortran')
            let l:FCompiler = 'pgfortran -O0 -Mbounds -Minform=inform -Mchkptr -Mstandard -Mchkstk -o %:r.exe %'
        elseif executable('ifort')
            let l:FCompiler = 'ifort -O0 -traceback -warn all -check bounds -check uninit -debug all -warn interface -fpe0 -o %:r.exe %'
        else
            let l:FCompiler = 'gfortran -O0 -g -Wall -Wextra -fbounds-check -o %:r.exe %'
        endif
    else
        if executable('pgfortran')
            let l:FCompiler = 'pgfortran'
        elseif executable('ifort')
            let l:FCompiler = 'ifort'
        else
            let l:FCompiler = 'gfortran'
        endif
    endif
    return l:FCompiler
endfunction

" Custom Compiling Options
function! CustomCompile()
    if has('win32')
        :!gfortran -g -Wall -Wextra -fbounds-check -o %:r.exe %
    elseif has('mac')
        :!ifort -g -no-pie -warn all -C -check all -ftrapuv -o %:r.exe %
    else
        let &l:makeprg = SelectFortranCompiler(1)
        " :!gfortran
    endif
endfunction

call CustomCompile()

nnoremap <F5> :make<CR>

if exists('g:neoterm_loaded') && g:neoterm_loaded
    nnoremap <buffer> <F6> :T ./%:r.exe<cr>
else
    nnoremap <buffer> <F6> :split term://./%:r.exe <cr>
endif
