setlocal textwidth=80
setlocal spell spelllang=en_gb
setlocal colorcolumn=81
" set complete+=kspell
let g:gitgutter_signs = 0

" ctags with latex
let g:tlist_tex_settings = 'latex;s:sections;g:graphics;l:labels'

" Set conceal
setlocal conceallevel=2

"let g:tex_conceal="adgms"
" a = conceal accents/ligatures
" d = conceal delimiters
" g = conceal Greek
" m = conceal math symbols
" s = conceal superscripts/subscripts
"
" Conceal hilightgroup
"hi Conceal guibg=#121212 guifg=darkmagenta

" The first tex file we open, will be the main
if !exists('g:tex_main')
    let g:tex_main = expand("%:t")
endif

if has('unix') && exists('g:loaded_slime') && g:loaded_slime
    nnoremap <buffer> <F5> <cmd>SlimeSend0 'pdflatex -synctex=1 ' . expand(g:tex_main) . "\r"<cr>
elseif exists('g:neoterm_loaded') && g:neoterm_loaded
    " nnoremap <buffer> <F5> <cmd>T pdflatex -synctex=1 <cr>
    " nnoremap <buffer> <F5> <cmd>call neoterm#exec({'cmd': ['pdflatex -synctex=1 ', expand(g:tex_main)]})<CR>
    nnoremap <buffer> <F5> <cmd>exec "T pdflatex -synctex=1 " . expand(g:tex_main) <cr>
else
    nnoremap <buffer> <F5> <cmd>split term://pdflatex -synctex=1 'thesis.tex' <cr>
endif

function! Synctex()
        " remove 'silent' for debugging
        " execute "silent !zathura --synctex-forward " . line('.') . ":" . col('.') . ":" . bufname('%') . " " . "thesis.pdf"
        execute "silent !qpdfview --unique thesis.pdf\\#src:%:" . line(".") .":0 &"
endfunction
nnoremap <buffer><F6> <cmd>call Synctex()<cr>
