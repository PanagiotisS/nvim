function! CalculateColourTint(colour, perc)
    " Inputs
    " colour: in HEX
    " perc: How much lighter/darker to be

    " Convert HEX to DEC
    let l:red = str2nr(a:colour[1:2], 16)
    let l:green = str2nr(a:colour[3:4], 16)
    let l:blue = str2nr(a:colour[5:6], 16)

    " Check if background is set to 'dark' or 'light'
    if &l:background ==? "dark"
        " Make it lighter: R|G|B + (255 - R|G|B) * perc
        let l:red = l:red + float2nr((255 - l:red) * a:perc)
        let l:green = l:green + float2nr((255 - l:green) * a:perc)
        let l:blue = l:blue + float2nr((255 - l:blue) * a:perc)
    else
        " Make it darker: R|G|B * perc
        let l:red = float2nr(l:red * (1.0 - a:perc))
        let l:green = float2nr(l:green * (1.0 - a:perc))
        let l:blue = float2nr(l:blue * (1.0 - a:perc))
    endif

    " Convert back DEC to HEX and return
    return printf("#%x%x%x", l:red, l:green, l:blue)
endfunction
