function! ToggleSpell()
    let l:myLangList=['nospell', 'en_gb', 'el']
    if !exists( 'b:myLang' )
        let b:myLang=0
    endif
    let b:myLang=b:myLang+1
    if b:myLang >= len(l:myLangList) | let b:myLang=0 | endif
    if b:myLang == 0
        setlocal nospell
    else
        execute 'setlocal spell spelllang='.get(l:myLangList, b:myLang)
    endif
    echo 'spell checking language:' l:myLangList[b:myLang]
endfunction
