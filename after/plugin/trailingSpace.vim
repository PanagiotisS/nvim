" http://stackoverflow.com/a/19936807
fun! StripTrailingWhiteSpace()
    " don't strip on these filetypes
    if &ft =~ 'markdown'
        return
    endif
    %s/\s\+$//ge
endfunction

autocmd BufWritePre * :call StripTrailingWhiteSpace()
