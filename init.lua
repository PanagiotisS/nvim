-- vim:fdm=marker:foldlevel=0
vim.o.encoding = "utf-8"

-- Set makeprg on windows {{{
if vim.fn.has("win32") == 1 then
    vim.o.makeprg = "mingw32-make"
end
-- }}}

-- Leader Remap {{{
-- Remap leader to space - Set it here before start using leader
vim.g.mapleader = " "
-- }}}

-- lazy.nvim {{{
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
vim.opt.rtp:prepend(lazypath)
-- }}}

-- Mappings {{{
-- Use escape in terminal
vim.keymap.set("t", "<ESC>", "<C-\\><C-n>", { desc = "Use escape in terminal" })

-- remove Ex mode shortcut
vim.keymap.set("n", "Q", "")

-- when wrap, go down/up by each visual line
vim.keymap.set("n", "<Down>", "gj")
vim.keymap.set("n", "j", "gj")

vim.keymap.set("n", "<up>", "gk")
vim.keymap.set("n", "k", "gk")

-- move between tabs
vim.keymap.set("n", "<C-J>", "<C-W><C-J>")
vim.keymap.set("n", "<C-K>", "<C-W><C-K>")
vim.keymap.set("n", "<C-L>", "<C-W><C-L>")
vim.keymap.set("n", "<C-H>", "<C-W><C-H>")

-- call ToggleSpell with F7
vim.keymap.set("n", "<F7>", "<cmd>call ToggleSpell()<CR>")
-- }}}

-- functions/augroups {{{
local nvim_augroup = vim.api.nvim_create_augroup("nvimrc", { clear = true })
-- Highlight on copy
vim.api.nvim_create_autocmd({ "TextYankPost" }, {
    pattern = "*",
    group = nvim_augroup,
    command = "lua require('vim.highlight').on_yank()",
})
-- Return to last edit position when opening files
vim.api.nvim_create_autocmd({ "BufReadPost" }, {
    pattern = "*",
    group = nvim_augroup,
    command = [[ if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif ]],
})
-- }}}
-- General Setting {{{
-- Do not keep buffers loaded on the background
vim.o.hidden = false

-- vim.o.lazyredraw = true

-- show the line number
vim.o.number = true

-- do not wrap long lines
vim.o.wrap = false

vim.o.modeline = true

-- Colour settings
vim.o.termguicolors = true

-- Show line and column
vim.o.ruler = true

-- Disalbe alt shortcuts for GUI version
vim.o.winaltkeys = "no"

vim.o.visualbell = true

-- Right click pops up a menu without moving the cursor
vim.o.mousemodel = "popup"
vim.o.mouse = "a"

-- Stay in the same column when scrolling
vim.o.startofline = false

-- set background based on the time of day
if os.date("*t")["hour"] >= 6 and os.date("*t")["hour"] < 17 then
    vim.o.background = "light"
else
    vim.o.background = "dark"
end
vim.cmd.colorscheme("quiet")

-- make backspace work like most other apps (default)
vim.o.backspace = "indent,eol,start"

vim.o.wildmenu = true
vim.o.wildmode = "longest:full,full"

-- Highlight search results (default)
vim.o.hlsearch = true

-- Show matching brackets when text indicator is over them (default)
vim.o.showmatch = true

-- Use spaces instead of tabs
vim.o.expandtab = true

-- Be smart when using tabs
vim.o.smarttab = true

-- fold
vim.o.foldlevel = 99

-- 1 tab == 4 spaces
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.softtabstop = 4

-- keep n lines visible when scrolling
vim.o.scrolloff = 10
vim.o.sidescrolloff = 5

-- save with W
-- cnoreabbrev in `:lua vim.key.set()` ?
vim.cmd("cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))")

-- Show pressed keys
vim.o.showcmd = true

vim.g.tex_flavor = "latex"

-- more natural split opening
vim.o.splitbelow = true
vim.o.splitright = true

-- Disable timout for pressed keys (like leader key)
vim.o.timeout = false
vim.o.ttimeout = true

-- Do not unfold if search found a match
vim.opt.foldopen:remove({ "search" })

-- Sets undofile
vim.o.undofile = true

-- stylua: ignore
vim.o.langmap = "ΑA,ΒB,ΨC,ΔD,ΕE,ΦF,ΓG,ΗH,ΙI,ΞJ,ΚK,ΛL,ΜM,ΝN,ΟO,ΠP,QQ,ΡR,ΣS,ΤT,ΘU,ΩV,WW,ΧX,ΥY,ΖZ,αa,βb,ψc,δd,εe,φf,γg,ηh,ιi,ξj,κk,λl,μm,νn,οo,πp,qq,ρr,σs,τt,θu,ωv,ςw,χx,υy,ζz"

-- completion options
vim.o.completeopt = "menuone,noinsert,noselect"
-- }}}
-- lazy.nvim {{{
local lazy_opts = {
    performance = {
        rtp = {
            paths = { vim.fn.stdpath("data") .. "/site" },
        },
    },
    ui = {
        icons = {
            cmd = "⌘",
            config = "🛠",
            event = "📅",
            ft = "📂",
            init = "⚙",
            keys = "🗝",
            plugin = "🔌",
            runtime = "💻",
            source = "📄",
            start = "🚀",
            task = "📌",
            lazy = "💤 ",
        },
    },
}
require("lazy").setup(require("my-plugins"), lazy_opts)
-- }}}
