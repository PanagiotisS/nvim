vim.filetype.add({
    -- Detect and assign filetype based on the extension of the filename
    extension = {
        cppm = "cpp",
        fi = "fortran",
        cuf = "fortran",
    },
})
